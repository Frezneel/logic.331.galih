package WarmUp;

import java.util.Scanner;

public class Soal01 {
    public static void resolve(){
        Scanner input = new Scanner(System.in);

        System.out.println("=======+ Solve Me First +=======");
        System.out.print("Masukkan Nilai A = ");
        int inputA = input.nextInt();
        System.out.print("Masukkan Nilai B = ");
        int inputB = input.nextInt();

        int output = inputA + inputB;
        System.out.print("Output = ");
        System.out.println(output);

        Utility.mauLagi();

    }
}
