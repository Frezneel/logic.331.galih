package WarmUp;

import java.util.Scanner;

public class Soal09 {
    public static void resolve(){
        Scanner input = new Scanner(System.in);

        System.out.println("=======+ A Very Big Sum +=======");
        System.out.println("Contoh input.");
        System.out.println("Input = 100001 100002 100003 100004 100005 100007");
        String inputString = input.nextLine();
        long jumlah = 0;

        String[] nilai = inputString.split(" ");
        long[] intBigNum = new long[nilai.length];
        for (int i = 0; i < nilai.length; i++) {
            intBigNum[i] = Long.parseLong(nilai[i]);
            jumlah += Long.parseLong(nilai[i]);
        }

        System.out.print("Output = ");
        System.out.println(jumlah);

        Utility.mauLagi();
    }
}
