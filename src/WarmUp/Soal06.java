package WarmUp;

import java.util.Scanner;

public class Soal06 {
    public static void resolve(){
        Scanner input = new Scanner(System.in);

        System.out.print("=====+ Staircase +=====");
        System.out.print("Masukkan Panjang Staircase (n) = ");
        int n = input.nextInt();
        String simbol = "#";

        String[][] result = new String[n][n];

        for (int i = 0; i < n; i++) {
            for (int j = 0; j < (n-(i+1)); j++) {
                System.out.print(" ");
            }
            for (int k = 0; k < (i+1); k++) {
                System.out.print(simbol);
            }
            System.out.println("");
        }

        Utility.mauLagi();
    }
}
