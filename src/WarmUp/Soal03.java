package WarmUp;

import java.util.Scanner;

public class Soal03 {
    public static void resolve(){
        Scanner input = new Scanner(System.in);

        System.out.println("=======+ Simple Array Sum +=======");
        System.out.println("Contoh input = 1 2 3 4 5 6");
        System.out.println("Masukkan Nilai = ");
        String inputA = input.nextLine();

        String[] nilai = inputA.split(" ");
        int convertInt = 0;
        for (int i = 0; i < nilai.length; i++) {
            convertInt += Integer.parseInt(nilai[i]);
        }

        System.out.print("Output = ");
        System.out.println(convertInt);
        Utility.mauLagi();
    }
}
