package WarmUp;

import java.util.Scanner;

public class Soal10 {
    public static void resolve(){
        Scanner input = new Scanner(System.in);
        System.out.println("===+ Compare The Triplets +===");
        System.out.println("Masukkan Deret Angka : ");
        System.out.println("Contoh : 1 2 3 4 5");
        System.out.println("Masukkan A: ");
        String inputNilaiA = input.nextLine();
        System.out.println("Masukkan B: ");
        String inputNilaiB = input.nextLine();
        int[] nilaiA = Utility.ConvertStringToArrayInt(inputNilaiA);
        int[] nilaiB = Utility.ConvertStringToArrayInt(inputNilaiB);
        int panjang = nilaiA.length;
        int pointA = 0;
        int pointB = 0;

        for (int i = 0; i < panjang; i++) {
            if (nilaiA[i] > nilaiB[i]){
                pointA ++;
            } else if (nilaiA[i] == nilaiB[i]) {
            } else {
                pointB ++;
            }
        }
        System.out.print("Output = [" + pointA + ", "+ pointB + "]");
        Utility.mauLagi();
    }
}
