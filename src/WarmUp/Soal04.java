package WarmUp;

import java.util.Scanner;

public class Soal04 {
    public static void resolve(){
        Scanner input = new Scanner(System.in);
        System.out.println("=======+ Diagonal Difference +=======");
        System.out.println("Contoh input.");
        System.out.println("Baris 1 = 1 2 3");
        System.out.println("Baris 2 = 4 5 6");
        System.out.println("Baris 3 = 7 8 9");
        System.out.println("Matrix berukuran n x n");
        System.out.println("input Contoh = 1 2 3 4 5 6 7 8 9");
        System.out.println("Masukkan Input = ");
        String matrixInput = input.nextLine();
        String[] matrix = matrixInput.split(" ");
        int nMatrix = (int) Math.sqrt(matrix.length);
        int[][] intMatrix = new int[nMatrix][nMatrix];
        int count = 0;
        for (int i = 0; i < nMatrix; i++) {
            for (int j = 0; j < nMatrix; j++) {
                intMatrix[i][j] = Integer.parseInt(matrix[count]);
                count++;
            }
        }
        int miringKanan = 0;
        int miringKiri = 0;
        int output = 0;
        if (intMatrix.length % 2 == 0) //Genap
        {
            for (int i = 0; i < nMatrix; i++) {
                for (int j = 0; j < nMatrix; j++) {
                    if (i == j){
                        miringKanan += intMatrix[i][j];
                    }else if (i + j == (nMatrix-1)){
                        miringKiri += intMatrix[i][j];
                    }
                }
            }
        }
        else //Ganjil Tanpa tengah 2x
        {
            for (int i = 0; i < nMatrix; i++) {
                for (int j = 0; j < nMatrix; j++) {
                    if (i == j){
                        miringKanan += intMatrix[i][j];
                    }else if (i + j == (nMatrix-1)){
                        miringKiri += intMatrix[i][j];
                    }
                }
            }
        }
        output = Math.abs(miringKanan - miringKiri);
        System.out.print("Output = " + output);
        Utility.mauLagi();
    }
}
