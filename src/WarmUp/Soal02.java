package WarmUp;

import java.util.Scanner;

public class Soal02 {
    public static void resolve(){

        Scanner input = new Scanner(System.in);

        System.out.println("=======+ Time Conversion +=======");
        System.out.println("input format (12AM/PM) :");
        System.out.println("hh:mm:ss:AM atau hh:mm:ss:PM");

        String inputDate = input.nextLine();

        String[] nilai = inputDate.split(":");

        int jam = Integer.parseInt(nilai[0]);
        String menit = nilai[1];
        String detik = nilai[2];
        String kondisi = nilai[3];

        if (kondisi.equals("PM")){
            System.out.println("Output jam : " + ((jam) + 12) + ":" + menit + ":" + detik);

        }else {
            System.out.println("Output jam : " + jam + ":" + menit + ":" + detik);
        }
        Utility.mauLagi();
    }
}
