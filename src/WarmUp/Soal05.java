package WarmUp;

import java.text.DecimalFormat;
import java.util.Scanner;

public class Soal05 {
    public static void resolve(){
        System.out.println("==== Plus Minus ====");
        System.out.println("Masukkan Deret Angka : ");
        System.out.println("Contoh : 1 2 3 4 5");
        System.out.println("Masukkan : ");
        Scanner input = new Scanner(System.in);

        String inputNilai = input.nextLine();

        String[] nilai = inputNilai.split(" ");

        int[] intNilai = new int[nilai.length];

        for (int i = 0; i < nilai.length; i++) {
            intNilai[i] = Integer.parseInt(nilai[i]);
        }

        int panjang = nilai.length;
        double positive = 0;
        double negative = 0;
        double zero = 0;

        for (int i = 0; i < panjang; i++) {
            if (intNilai[i] < 0){
                negative++;
            }
            if (intNilai[i] == 0){
                zero++;
            }if (intNilai[i] > 0){
                positive++;
            }
        }
        negative = (negative/panjang) ;
        positive = (positive/panjang) ;
        zero = (zero/panjang) ;

        System.out.println("Positive = " + positive);
        System.out.println("Negative = " + negative);
        System.out.println("Zero = " + zero);

        Utility.mauLagi();
    }
}
