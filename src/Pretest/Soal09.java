package Pretest;

import java.util.Scanner;

public class Soal09 {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        System.out.println("==== HATORI NINJA ====");
        System.out.println("Contoh input = N N T N N N T T T T T N T T T N T N");
        String inputString = input.nextLine();
        inputString = inputString.trim().toUpperCase();
        char[] inputChar = inputString.toCharArray();

        int hitungN = 0;
        int hitungT = 0;
        int gunung = 0;
        int lembah = 0;
        char sebelum;

        for (int i = 0; i < inputChar.length; i++) {
            char validChar = inputChar[i];
            if (validChar == 'N'){
                sebelum = 'N';
                hitungN ++;
                if (hitungN == hitungT){
                    lembah ++;
                    hitungN = 0;
                    hitungT = 0;
                }
            } else if (validChar == 'T') {
                sebelum = 'T';
                hitungT ++;
                if (hitungN == hitungT){
                    gunung ++;
                    hitungN = 0;
                    hitungT = 0;
                }
            }
        }
        System.out.println("cek = " + hitungN + " | " + hitungT);
        System.out.println("Output");
        System.out.println("Gunung = " + gunung);
        System.out.println("Lembah = " + lembah);

    }
}
