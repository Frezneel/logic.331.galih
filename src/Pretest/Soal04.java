package Pretest;

import java.util.Scanner;

public class Soal04 {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);

        String[] namaTempat = {"toko", "tempat 1", "tempat 2", "tempat 3", "tempat 4"};
        double[] jarakTempat = {0, 2, 2.5, 4, 6.5};

        System.out.println("====== Perhitungan Bensin ======");
        System.out.println("contoh input : Toko - Tempat 1 - Tempat 2 - Toko");
        System.out.println("Masukkan rute toko");
        String inputString = input.nextLine().toLowerCase();
        String[] splitInput = inputString.split(" - ");

        int[] namaToInt = new int[splitInput.length];
        for (int i = 0; i < splitInput.length; i++) {
            for (int j = 0; j < namaTempat.length; j++) {
                if (splitInput[i].equals(namaTempat[j])){
                    namaToInt[i] = j;
                }
            }
        }
        //Olah Data
        double jarak = 0;
        double tokoSebelum = 0;

        for (int i = 0; i <splitInput.length; i++) {
            int toko = namaToInt[i];
            double tokoSekarang = Math.abs(jarakTempat[toko] - tokoSebelum);
            jarak += tokoSekarang;
            tokoSebelum = jarakTempat[toko];
        }

        // 2km + 500m + 2,5km

        // output
        double output = jarak / 2.5;
        System.out.println("jarak = " + jarak);
        System.out.println("Output = " + output + " liter");


    }
}
