package Pretest;

import java.util.Scanner;

public class Soal10 {
    public static void main(String[] args) {
        // Harga cup = 18000
        Scanner input = new Scanner(System.in);
        System.out.println("===== Kafe Kopi =====");
        System.out.println("Masukkan uang = ");
        double uangInput = input.nextInt();
        double hargaKopi = 18000;

        boolean flag = true;
        int countKopi = 1;
        double hargaTerdekat = 0;
        double hargaSebelumnya = 0;
        double hargaDiskon = 0;
        while (flag){
            if (uangInput > hargaDiskon){
                hargaSebelumnya = hargaTerdekat;
                hargaTerdekat = hargaKopi * countKopi;
                if (hargaTerdekat > 40000){
                    double nilaiDiskon50 = hargaTerdekat * 0.5;
                    if (nilaiDiskon50 >= 100000){
                        nilaiDiskon50 = 100000;
                    }
                    hargaDiskon = hargaTerdekat - nilaiDiskon50;
                }
                hargaTerdekat = hargaDiskon;
                double cashback30 = hargaTerdekat * 0.1;
                if (cashback30 > 30000){
                    cashback30 = 30000;
                }
                hargaTerdekat = hargaTerdekat - cashback30;
                countKopi++;
            }else{
                hargaTerdekat = hargaSebelumnya;
                countKopi -= 2;
                flag = false;
            }
        }
        int saldoAkhir = (int) (uangInput - hargaTerdekat);
        System.out.println("Output");
        System.out.println("Jumlah cup = " + countKopi);
        System.out.println("Saldo akhir = " + saldoAkhir);

    }
}
