package Pretest;

import java.util.Scanner;

public class Soal08 {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        System.out.println("Penjumlahan deret Prima dan Fibonaci");
        System.out.print("Masukkan panjang deret = ");
        int inputPanjang  = input.nextInt();

        // Buat deret angka primary
        // i = patokan = j pembagi
        // i % j == 0 ? berapa kali 0 nya? prima itu hanya punya 2x saja
        int[] deretPrimary = new int[inputPanjang];
        int countBagi = 0;
        int countIsi = 0;
        int cariPrimary = 3;
        for (int i = 2; i < cariPrimary; i++) {
            // Pembagian i % j == 0 berapa x??
            for (int j = 1; j <= i; j++) {
                if (i % j == 0){
                    countBagi++;
                }
            }
            // kalau benar 2x masukkan ke data dan tambahkan hitung isinya
            if (countBagi == 2){
                deretPrimary[countIsi] = i;
                countIsi++;
            }
            // kalau isi dari tampungan deretPrimary sudah sepanjang length input maka berhenti.
            if (countIsi == inputPanjang){
                break;
            }else {
                cariPrimary ++;
            }
            // reset ke 0
            countBagi = 0;
        }

        // Buat deret fibonaci
        int[] deretFibonaci = new int[inputPanjang];
        int awal = 1;
        int sebelum = 0;
        for (int i = 0; i < inputPanjang; i++) {
            if (i == 0){
                deretFibonaci[i] = awal;
            }else {
                int sekarang = deretFibonaci[i-1];
                deretFibonaci[i] = sebelum + sekarang;
                sebelum = sekarang;
            }
        }
        // Olah penjumlahan deret primary dan fibonaci
        System.out.println("Output = ");
        int[] output = new int[inputPanjang];
        for (int i = 0; i < output.length; i++) {
            if (i == output.length - 1){
                output[i] = deretPrimary[i] + deretFibonaci[i];
                System.out.print(deretPrimary[i] + "+" + deretFibonaci[i]);
            }else {
                output[i] = deretPrimary[i] + deretFibonaci[i];
                System.out.print(deretPrimary[i] + "+" + deretFibonaci[i] + " ; ");
            }
        }
    }
}
