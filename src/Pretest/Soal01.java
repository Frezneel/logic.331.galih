package Pretest;

import java.util.Scanner;

public class Soal01 {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        System.out.println("==== ganjil genap ====");
        System.out.println("Masukkan panjang deret : ");
        int n = input.nextInt();
        int awalGanjil = 1;
        int awalGenap = 2;

        System.out.println("Output = ");
        for (int i = 0; i < 2; i++) {
            for (int j = 0; j < n; j++) {
                if (i == 0){
                    System.out.print(awalGanjil + " ");
                    awalGanjil += 2;
                    if (awalGanjil > n){
                        break;
                    }
                }else {
                    System.out.print(awalGenap + " ");
                    awalGenap += 2;
                    if (awalGenap > n){
                        break;
                    }
                }

            }
            System.out.println();
        }

    }
}
