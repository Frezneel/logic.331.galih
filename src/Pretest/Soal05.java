package Pretest;

import java.util.HashMap;
import java.util.Scanner;

public class Soal05 {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        System.out.println("==== HITUNG PORSI ====");
        System.out.println("Contoh input");
        System.out.println("Laki-laki dewasa = 3 orang; Perempuan dewasa = 1 orang; Balita = 1 orang; Laki-laki dewasa = 1 orang;");
        System.out.println("Masukkan input = ");
        String inputString = input.nextLine();

        String[] inputSplit = inputString.split("; ");

        HashMap<String, Integer> pelanggan = new HashMap<String, Integer>();

        // split sebelum masuk hashmap
        String tipePelanggan = "";
        int banyakOrang = 0;
        int jumlahPelanggan = 0;
        for (int i = 0; i < inputSplit.length; i++) {
            String[] split = inputSplit[i].split(" = ");
            for (int j = 0; j < split.length; j++) {
                if (j == 0){
                    tipePelanggan = split[j];
                }else {
                    banyakOrang = Integer.parseInt(split[j].substring(0,1));
                    jumlahPelanggan += banyakOrang;
                }
            }
            boolean cek = pelanggan.containsKey(tipePelanggan);
            if (cek){
                int angkaLama = pelanggan.get(tipePelanggan);
                int angkaBaru = angkaLama + banyakOrang;
                pelanggan.replace(tipePelanggan, angkaLama, angkaBaru);
            }else {
                pelanggan.put(tipePelanggan, banyakOrang);
            }
        }

        // ambil value pakai contains
        double porsi = 0;
        boolean wanitaTambah = false;
        if (jumlahPelanggan % 2 != 0 && jumlahPelanggan > 5){
            wanitaTambah = true;
        }
        for (String nama : pelanggan.keySet()) {
            if (nama.contains("Laki-laki dewasa")){
                porsi += 2 * pelanggan.get(nama);
            } else if (nama.contains("Perempuan dewasa")) {
                if (wanitaTambah){
                    porsi += 2 * pelanggan.get(nama);
                }else {
                    porsi += 1 * pelanggan.get(nama);
                }
            } else if (nama.contains("Remaja")) {
                porsi += 2 * pelanggan.get(nama);
            }else if (nama.contains("anak-anak")){
                porsi += 0.5 * pelanggan.get(nama);
            } else if (nama.contains("Balita")) {
                porsi += 1 * pelanggan.get(nama);
            }
        }

        System.out.println("Output");
        System.out.println(porsi + " porsi");
    }
}
