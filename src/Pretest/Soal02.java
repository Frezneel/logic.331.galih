package Pretest;

import java.util.Scanner;

public class Soal02 {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        char[] hurufVokal = ("aiueo").toCharArray();
        char[] hurufKonsonan = ("bcdfghjklmnpqrstvwxyz").toCharArray();

        System.out.println("==== Program pimilah huruf vokal dan konsonan ====");
        System.out.println("Masukkan kata/kalimat = ");
        String inputString = input.nextLine().toLowerCase();
        inputString = inputString.trim();

        char[] inputChar = inputString.toCharArray();

        String outputVokal = "";
        String outputKonsonan = "";
        for (int i = 0; i < hurufVokal.length; i++) {
            for (int j = 0; j < inputChar.length; j++) {
                if (hurufVokal[i] == inputChar[j]){
                    outputVokal += hurufVokal[i];
                }
            }
        }

        for (int i = 0; i < hurufKonsonan.length; i++) {
            for (int j = 0; j < inputChar.length; j++) {
                if (hurufKonsonan[i] == inputChar[j]){
                    outputKonsonan += hurufKonsonan[i];
                }
            }
        }

        System.out.println("Output ");
        System.out.println("Vokal = " + outputVokal);
        System.out.println("Konsonan = " + outputKonsonan);

    }

}
