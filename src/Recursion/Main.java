package Recursion;


import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        menu();
    }
    public static void menu(){
        Scanner input = new Scanner(System.in);
        System.out.println("===== MENU =====");
        System.out.println("SOAL STRING MANIPULATION");
        System.out.println("Masukkan nomor soal dari 1 - 10");
        int pilihan = input.nextInt();
        while (pilihan < 1 || pilihan > 12){
            System.out.println("Pilihan tidak sesuai, Pastikan sesuai dengan Menu!");
            System.out.println("Pilihan : ");
            pilihan = input.nextInt();
        }

        switch (pilihan){
            case 1 :
                DigitSum01.resolve();
                break;
            case 2 :

                break;
            case 3 :

                break;
            case 4 :

                break;
            case 5 :

                break;
            case 6 :

                break;
            case 7 :

                break;
            case 8 :

                break;
            case 9 :

                break;
            case 10 :

                break;
            default:
        }
    }
}
