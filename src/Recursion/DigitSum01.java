package Recursion;

import java.util.Scanner;

public class DigitSum01 {
    public static void resolve(){
        Scanner input = new Scanner(System.in);

        System.out.println("Masukkan angka dan banyak perulangan : ");
        System.out.println("Contoh : angka 148 sebanyak 3x");
        System.out.println("Input = 148 3");
        System.out.println("Masukkan : ");
        String inputNumber = input.nextLine();
        String[] split = inputNumber.split(" ");
        int ulangi = Integer.parseInt(split[1]);
        String angka = "";

        for (int i = 0; i < ulangi; i++) {
            angka += split[0];
        }
        superDigits(angka);
    }


    private static String superDigits (String digits){
        int summary = 0;
        String tampung ="";
        for (int i = 0; i < digits.length(); i++) {
            if(i == 0){
                tampung = digits.substring(0,1);
                summary += Integer.parseInt(tampung);
            }else {
                tampung = digits.substring(i, i+1);
                summary += Integer.parseInt(tampung);
            }
        }
        digits = Integer.toString(summary);
        if (digits.length() < 2){
            System.out.print("Output = ");
            System.out.println(summary);
            return digits;
        }

        return superDigits(digits);
    }
}
