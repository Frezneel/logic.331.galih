package Filtering1;

import java.util.Scanner;

public class Soal09 {//Selesai | output masih ada , dibelakang
    public static void resolve(){
        Scanner input = new Scanner(System.in);
        System.out.println("Masukkan kalimat : ");
        String inputString = input.nextLine();
        String[] inputArray = inputString.split(" ");
        int[] panjangKata = new int[inputArray.length];
        int panjangTerbesar = 0;

        for (int i = 0; i < panjangKata.length; i++) {
            panjangKata[i] = inputArray[i].length();
            if (panjangTerbesar < panjangKata[i]){
                panjangTerbesar = panjangKata[i];
            }
        }

        System.out.println("Output:");
        int data = 0;
        for (int i = 0; i <= panjangTerbesar; i++) {
            for (int j = 0; j < panjangKata.length; j++) {
                if (panjangKata[j] == i){
                    System.out.print(inputArray[j] + ",");
                    data ++;
                }
            }
            if(data > 0){
                System.out.println();
                data = 0;
            }
        }

        Main.soalLain();
    }
}
