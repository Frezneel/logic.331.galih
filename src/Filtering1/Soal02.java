package Filtering1;

import java.util.Scanner;

public class Soal02 {
    public static void resolve(){ //Selesai
        String simbol = "~!@#$%^&*()_+-=:;<>/?";
        Scanner input = new Scanner(System.in);
        System.out.println("Masukkan username : ");
        String inputString = input.nextLine();
        boolean isSimbol = false;

        for (int i = 0; i < simbol.length(); i++) {
            String ambilSimbol = simbol.substring(i,i+1);
            if (inputString.contains(ambilSimbol)){
                isSimbol = true;
                break;
            }
        }
        System.out.println("Output = ");

        if (isSimbol){
            System.out.println(" ");
        }else {
            if (inputString.length() < 5 && inputString.length() > 10) {
                System.out.println("invalid");
            } else if (!inputString.contains("_")) {
                System.out.println("invalid");
            } else {
                System.out.println("valid");
            }
        }
        Main.soalLain();
    }
}
