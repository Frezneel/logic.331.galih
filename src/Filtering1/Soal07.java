package Filtering1;

import java.util.Scanner;

public class Soal07 { // Done
    public static void resolve(){
        String simbol = ",'\"@/&!";
        Scanner input = new Scanner(System.in);
        System.out.println("Masukkan kalimat = ");
        String inputString = input.nextLine();
        for (int i = 0; i < simbol.length(); i++) {
            String simbolReplace = simbol.substring(i, i+1);
            inputString = inputString.replaceAll(simbolReplace, "");
        }

        String[] cekKata = inputString.split(" ");
        String output = "";
        for (int i = 0; i < cekKata.length; i++) {
            boolean isSame = false;
            for (int j = 0; j < i; j++) {
                if (cekKata[i].equals(cekKata[j])){
                    isSame = true;
                    break;
                }
            }
            if (!isSame){
                output += cekKata[i] + " ";
            }
        }

        System.out.println(output);
        Main.soalLain();
    }
}
