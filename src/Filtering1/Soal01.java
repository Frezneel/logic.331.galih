package Filtering1;

import java.util.Scanner;

public class Soal01 { //Selesai
    public static void resolve(){
        Scanner input = new Scanner(System.in);
        System.out.println("Maskkan deret angka : ");
        String inputString = input.nextLine();
        String[] inputArray = inputString.split(" ");
        String outputString = "";
        int output = 0;

        System.out.println("Output");
        for (int i = 0; i < inputArray.length; i++) {
            if (i > 0){
                output += Integer.parseInt(inputArray[i]);
                outputString += " + " + output;
                System.out.print(outputString + " = " + output);
            }else if (i == 0){
                output = Integer.parseInt(inputArray[i]);
                outputString += "" + output;
                System.out.println(output);
            }
            System.out.println();
        }

        Main.soalLain();
    }
}
