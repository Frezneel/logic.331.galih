package Filtering1;

import java.text.DecimalFormat;
import java.util.Random;
import java.util.Scanner;

public class Soal05 {
    public static void resolve(){ // Selesai
        Scanner input = new Scanner(System.in);
        System.out.println("Masukkan total penduduk n : ");
        int banyakN = input.nextInt();
        System.out.println("Masukkan banyak calon legislatif m : ");
        int banyakM = input.nextInt();
        Random random = new Random();
        int[] suaraN = new int[banyakM + 1];
        for (int i = 0; i < banyakN; i++) {
            suaraN[random.nextInt(banyakM + 1)] ++;
        }
        String[][] printCalon = new String[2][banyakM + 1];
        for (int i = 0; i < suaraN.length; i++) {
            int urutan = banyakM;
            int ambilSuara = suaraN[i];
            for (int j = 0; j < suaraN.length; j++) {
                if (ambilSuara > suaraN[j]){
                    urutan--;
                }
            }
            printCalon[0][i] = String.valueOf(urutan);
        }

        System.out.println();

        DecimalFormat df = new DecimalFormat("0.00");
        for (int i = 0; i < suaraN.length; i++) {
            double persen = ((double) suaraN[i] / banyakN) * 100;
            if (i == 0){
                System.out.println();
                printCalon[1][i] = "Golput: " + suaraN[i] + " Suara ("+df.format(persen)+"%)";
            }else {
                printCalon[1][i] = "Calon no. urut "+ i + ": " + suaraN[i] + " Suara ("+df.format(persen)+"%)";
            }
        }

        for (int i = 0; i < printCalon[1].length; i++) {
            for (int j = 0; j < printCalon[0].length; j++) {
                int ambilPosisi = Integer.parseInt(printCalon[0][j]);
                if (ambilPosisi == i){
                    System.out.println(printCalon[1][j]);
                }
            }
        }
        Main.soalLain();
    }
}
