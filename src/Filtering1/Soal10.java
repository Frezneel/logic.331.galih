package Filtering1;

import java.util.Scanner;

public class Soal10 {
    public static void resolve(){ //Selesai
        Scanner input = new Scanner(System.in);
        char[] hurufVokal = "aiueo".toCharArray();
        char[] hurufKonsonan = "bcdfghjklmnpqrstvwxyz".toCharArray();
        System.out.println("Masukkan kalimat : ");
        String inputString = input.nextLine();
        inputString = inputString.replace(" ", "");
        char[] inputChar = inputString.toLowerCase().toCharArray();
        boolean isVokal = false;
        int output = 0;

        for (int i = 0; i < inputChar.length; i++) {
            boolean isVokalNow = false;
            boolean lanjutKonsonan = false;
            char hurufInput = inputChar[i];
            for (int j = 0; j < hurufVokal.length; j++) {
                if (hurufInput == hurufVokal[j]){
                    isVokalNow = true;
                    isVokal = true;
                    break;
                }
            }
            if (!isVokalNow){
                for (int j = 0; j < hurufKonsonan.length; j++) {
                    if (hurufInput == hurufKonsonan[j]){
                        lanjutKonsonan = true;
                        break;
                    }
                }
            }
            if (isVokal && lanjutKonsonan){
                isVokal = false;
                output++;
            }
        }

        System.out.println("Output = " +output);
        Main.soalLain();
    }
}
