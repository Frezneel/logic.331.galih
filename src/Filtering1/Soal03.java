package Filtering1;

import java.util.Scanner;

public class Soal03 {
    public static void resolve(){
        String simbol = "~!@#$%^&*()_+-=:;<>/, ?";
        Scanner input = new Scanner(System.in);
        System.out.println("Masukkan kata atau kalimat");
        String kalimatInput = input.nextLine();
        System.out.println("Masukkan panjang penggalan kata");
        int inputN = input.nextInt();
        for (int i = 0; i < simbol.length(); i++) {
            String ambilSimbol = simbol.substring(i,i+1);
            kalimatInput = kalimatInput.replace(ambilSimbol, "");
        }
        System.out.println(kalimatInput);
        boolean flag = true;
        String output = "";
        int count = 0;
        while (flag){
            if (count == kalimatInput.length() / inputN){
                flag = false;
            }else{
                output += ", ";
            }
            output += kalimatInput.substring(inputN*count, inputN);
            count ++;
        }
        System.out.println(output);
        Main.soalLain();
    }
}
