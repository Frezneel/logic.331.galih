package Filtering1;

import java.util.Random;
import java.util.Scanner;

public class Soal06 {
    public static void resolve(){ // Selesai
        Scanner input = new Scanner(System.in);
        int[] angkaKotak = {3,5,3,5,8,5,3,5,3};

        System.out.println("Pilih player A atau B yang akan menang?");

        String inputPlayer = input.nextLine();
        String enemy = "";
        if (inputPlayer.equals("A")){
            enemy = "B";
        }else {
            enemy = "A";
        }

        int playerWin = 0;
        Random random = new Random();
        for (int i = 0; i < 3; i++) {
            System.out.print("Round "+ (i+1) +" => ");
            int posisiA = random.nextInt(8) + 1;
            int posisiB = random.nextInt(8) + 1;
            System.out.println("A = "+ posisiA +",B = " + posisiB);

            int nilaiA = angkaKotak[posisiA];
            int nilaiB = angkaKotak[posisiB];
            System.out.println("Nilai A = "+ nilaiA +",B = " + nilaiB);
            if (inputPlayer.equals("A") && nilaiA > nilaiB){
                playerWin ++;
            } else if (inputPlayer.equals("B") && nilaiA > nilaiB) {
                playerWin ++;
            }
        }

        if (playerWin >= 2){
            System.out.println("Anda Menang, " + enemy + " Kalah");
        }else {
            System.out.println("Anda Kalah, " + enemy + " Menang");
        }
        Main.soalLain();
    }
}
