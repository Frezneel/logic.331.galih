package Filtering1;

import java.util.Scanner;

public class Main {
    public static Scanner input = new Scanner(System.in);
    public static void main(String[] args) {
        menu();
    }
    public static void menu(){
        System.out.println("===== MENU SOAL =====");
        System.out.println("Masukkan nomor soal dari 1 - 10");
        int pilihan = input.nextInt();
        while (pilihan < 0 || pilihan > 10){
            System.out.println("Pilihan tidak sesuai, Pastikan sesuai dengan Menu!");
            System.out.println("Pilihan : ");
            pilihan = input.nextInt();
        }
        //bug
        input.nextLine();

        switch (pilihan){
            case 1 :
                Soal01.resolve();
                break;
            case 2 :
                Soal02.resolve();
                break;
            case 3 :
                Soal03.resolve();
                break;
            case 4 :
                Soal04.resolve();
                break;
            case 5 :
                Soal05.resolve();
                break;
            case 6 :
                Soal06.resolve();
                break;
            case 7 :
                Soal07.resolve();
                break;
            case 8 :
                Soal08.resolve();
                break;
            case 9 :
                Soal09.resolve();
                break;
            case 10 :
                Soal10.resolve();
                break;
            default:

        }
    }

    public static void soalLain(){
        boolean flag = true;
        while (flag){
            System.out.println("Cek soal lain ? Y/N");
            String pilihan = input.nextLine().toUpperCase();
            if (pilihan.equals("Y")){
                flag = false;
                menu();
            }else if (pilihan.equals("N")){
                flag = false;
                System.exit(0);
            }else {
                System.out.println("Input tidak sesuai, masukkan lagi!");
            }
        }
    }
}
