package Filtering1;

import java.util.Scanner;

public class Soal04 {
    public static void resolve(){ //Selesai
        Scanner input = new Scanner(System.in);
        System.out.print("Masukkan n = ");
        int inputN = input.nextInt();
        int banyakHalaman = inputN - 2;
        System.out.print("Masukkan x = ");
        int inputX = input.nextInt();
        while (inputX > banyakHalaman){
            System.out.println("sesuaikan dengan jumlah halaman n");
            System.out.println("Masukkan x = ");
            inputX = input.nextInt();
        }

        int lembar = inputX/2;
        int outputLembar = 0;

        if (inputX % 2 != 0){
            outputLembar = lembar + 1;
        }else {
            outputLembar = lembar;
        }
        System.out.println("Output");
        System.out.println("Lembar ke-" + outputLembar);
        Main.soalLain();
    }
}
