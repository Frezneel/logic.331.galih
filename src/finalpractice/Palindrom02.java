package finalpractice;

import java.util.Scanner;

public class Palindrom02 {
    public static void resolve(){ // 15 menit
        Scanner input = new Scanner(System.in);
        System.out.println("=====> Palindrome <======");
        System.out.print("Input = ");
        String inputString = input.nextLine();

        String pembandingInput = "";
        int subString1 = inputString.length();
        int subString2 = inputString.length()-1;


        for (int i = 0; i < inputString.length(); i++) {
            pembandingInput += inputString.substring(subString2-i, subString1-i);
        }

        String output = "";
        if (inputString.equals(pembandingInput)){
            output = "YES";
        }else {
            output = "NO";
        }

        System.out.println("Output = " + output);
    }
}
