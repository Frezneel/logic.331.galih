package finalpractice;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Scanner;

public class formatWaktu05 {

    public static void resolve(){
        Scanner input = new Scanner(System.in);

        System.out.println("Konversi Jam Format 12 Jam & 24 Jam");
        System.out.println("Contoh input 12 jam : 03:40:44 PM ");
        System.out.println("Contoh input 24 jam : 15:40:44 PM ");
        System.out.println("Input : ");
        String inputWaktu = input.nextLine();
        String ambilWaktu = inputWaktu.substring(0, 8);
        int jam = Integer.parseInt(inputWaktu.substring(0, 2));
        Date outputWaktu = null;

        System.out.println("Output");
        if (inputWaktu.contains("PM") || inputWaktu.contains("AM")){
            DateFormat df24 = new SimpleDateFormat("HH:mm:ss");
            if (inputWaktu.contains("PM")){
                try {
                    outputWaktu = df24.parse(ambilWaktu);
                    outputWaktu.setHours(jam + 12);
                    System.out.println(df24.format(outputWaktu));
                } catch (ParseException e) {
                    System.out.println("Format tidak sesuai");
                }
            }else {
                try {
                    outputWaktu = df24.parse(ambilWaktu);
                    System.out.println(df24.format(outputWaktu));
                } catch (ParseException e) {
                    System.out.println("Format tidak sesuai");
                }
            }
        }else {
            DateFormat df12 = new SimpleDateFormat("hh:mm:ss");
            if (jam < 12){
                try {
                    outputWaktu = df12.parse(ambilWaktu);
                    System.out.println(df12.format(outputWaktu) + " AM");
                } catch (ParseException e) {
                    System.out.println("Format tidak sesuai");
                }
            }else {
                try {
                    outputWaktu = df12.parse(ambilWaktu);
                    System.out.println(df12.format(outputWaktu) + " PM");
                } catch (ParseException e) {
                    System.out.println("Format tidak sesuai");
                }
            }
        }
    }

}
