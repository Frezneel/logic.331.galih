package ProblemSolving14;

import java.util.Arrays;
import java.util.Scanner;

public class Soal06 {
    public static void resolve(){
        Scanner input = new Scanner(System.in);
        System.out.println("+=====+ Pesanan Online +=====+");
        System.out.println("Masukkan tanggal dan hari Pemesanan : ");
        System.out.println("Contoh : 25 sabtu");
        System.out.println("input : ");
        String inputPemesanan = input.nextLine().toLowerCase();
        System.out.println("Masukkan hari libur nasional : ");
        System.out.println("Contoh : 26, 29");
        System.out.println("input : ");
        String inputLibur = input.nextLine();
        String[] dataPemesanan = inputPemesanan.split(" ");

        // Split dan sorting ke data int
        String[] dataLibur = inputLibur.split(", ");
        int[] sortDataLibur = new int[dataLibur.length];
        for (int i = 0; i < sortDataLibur.length; i++) {
            sortDataLibur[i] = Integer.parseInt(dataLibur[i]);
        }
        Arrays.sort(sortDataLibur);

        // Ambil nilai hari
        String[] dataHari = {"minggu", "senin", "selasa", "rabu", "kamis", "jumat", "sabtu"};
        String dataAwalHari = dataPemesanan[1];
        int awalHari = 0;
        for (int i = 0; i < dataHari.length; i++) {
            if (dataAwalHari.equals(dataHari[i])){
                awalHari = i;
                break;
            }
        }
        int estimasi = 7;
        int tglAwalMasuk = Integer.parseInt(dataPemesanan[0]);
        int mulaiHari = awalHari + 1;
        int tglProses = Integer.parseInt(dataPemesanan[0]) + 1;
        int count = 0;
        for (int i = 0; i < estimasi; i++) {
            if (mulaiHari >= 7){
                mulaiHari -= 7;
            }
            int tglLibur = sortDataLibur[count];
            if (tglProses == tglLibur){
                estimasi ++;
                if (dataLibur.length - 1 > count){
                    count ++;
                }
            } else if (mulaiHari == 0){
                estimasi ++;
            }
            mulaiHari ++;
            tglProses ++;
        }
        estimasi += tglAwalMasuk + 1; //besoknya dianter
        if (estimasi > 31){
            estimasi -= 31;
            System.out.println("OUTPUT = Tanggal " + estimasi + " di bulan berikutnya");
        }else {
            System.out.println("OUTPUT = Tanggal " + estimasi + " Hari " + dataHari[mulaiHari]);
        }
    }
}
