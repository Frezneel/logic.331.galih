package ProblemSolving14;

import java.util.Scanner;

public class Soal02 {
    public static void resolve(){
        Scanner input = new Scanner(System.in);
        System.out.println("+=====+ Grosir X +=====+");
        System.out.println("Contoh input : 1-2-3-4-5");
        System.out.println("Masukkan data penjualan : ");
        String data = input.nextLine();
        String[] dataString = data.split("-");
        int[] dataInt = new int[dataString.length];

        for (int i = 0; i < dataInt.length; i++) {
            dataInt[i] = Integer.parseInt(dataString[i]);
        }

        // Grosirx, toko1, toko2, toko3, toko4
        double[] dataToko = {0, 0.5 , 2 , 3.5 , 5};
        // Memberikan nilai pindah total
        double nilaiPindah = 0;
        // Ambil Nilai jarak antar titik Sebelumnya dengan setelahnya
        double jarak = 0;
        // Mengambil nilai jarak sebulmnya tanpa dikurangi
        double sebelumnya = 0;
        // Mengambil nilai posisi pada data input
        int pindah = 0;

        for (int i = 0; i < dataInt.length; i++) {
            pindah = dataInt[i];
            jarak = dataToko[pindah] - sebelumnya;
            sebelumnya = dataToko[pindah];
            nilaiPindah += Math.abs(jarak);
        }

        double output = dataToko[pindah] + nilaiPindah;
        double kecepatan = 0.5; // Per Menit bukan jam
        double delayTokoTotal = 10 * dataInt.length;
        double waktu = (output / kecepatan) + delayTokoTotal;

        System.out.println("nilai = " + waktu + " Menit");
    }
}
