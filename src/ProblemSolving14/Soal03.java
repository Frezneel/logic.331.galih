package ProblemSolving14;

import java.util.Arrays;
import java.util.HashMap;
import java.util.Scanner;

public class Soal03 {
    public static void resolve(){
        Scanner input = new Scanner(System.in);
        // INPUT
        System.out.println("+=====+ Summary Penjualan +=====+");
        System.out.println("Masukkan data penjualan : ");
        System.out.println("Contoh input : Apel:1, Pisang:3, Jeruk:1, Apel:3, Apel:5, Jeruk:8, Mangga:1");
        String data = input.nextLine();
        String[] dataArray = data.split(",");
        String[][] dataBuah2D = new String[2][dataArray.length];

        for (int i = 0; i < dataArray.length ; i++) {
            String[] split = dataArray[i].trim().split(":");
            for (int j = 0; j < split.length; j++) {
                dataBuah2D[j][i] = split[j];
            }
        }

        //Hashmap
        HashMap<String, Integer> buah = new HashMap<String, Integer>();

        // Mengulang Input untuk dimasukkan 2D
        for (int i = 0; i < dataBuah2D[0].length; i++) {
            String buahCek = dataBuah2D[0][i];
            // System.out.println("cek : " + buah.containsKey(buahCek));
            // untuk cek apakah ada di hashmap
            if (buah.containsKey(buahCek)){
                int dataSebelumnya = buah.get(buahCek);
                int dataAmbil = Integer.parseInt(dataBuah2D[1][i]);
                int nilaiBaru = dataSebelumnya + dataAmbil;
                buah.replace(buahCek, dataSebelumnya, nilaiBaru);
            }

            else {
                int angka = Integer.parseInt(dataBuah2D[1][i]);
                buah.put(buahCek, angka);
            }
        }

        // Print
        String[] sortString = new String[buah.size()];
        int index = 0;
        for (String key : buah.keySet())
        {
            sortString[index] = key + " : " + buah.get(key);
            index++;
        }

        Arrays.sort(sortString);

        //Ngeprint Nilai yang sudah disort berdasarkan Ascending
        for (int i = 0; i < sortString.length; i++) {
            System.out.println(sortString[i]);
        }

    }
}
