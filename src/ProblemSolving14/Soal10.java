package ProblemSolving14;

import java.util.Scanner;

public class Soal10 {
    private static String[] dataWadah = {"teko", "botol", "gelas", "cangkir"};

    public static void resolve() {
        Scanner input = new Scanner(System.in);

        System.out.println("+=======+ Program Konversi Wadah +======+");
        System.out.println("Pilih Wadah yang akan dikonversi = ");
        System.out.println("1. Teko");
        System.out.println("2. Botol");
        System.out.println("3. Gelas");
        System.out.println("4. Cangkir");
        System.out.println("Jawab = ");
        int jenis = input.nextInt();
        jenis -= 1;
        System.out.println("Masukkan banyak Wadah = ");
        int banyak = input.nextInt();

        System.out.println("Ingen dikonversi ke ?");
        System.out.println("1. Teko");
        System.out.println("2. Botol");
        System.out.println("3. Gelas");
        System.out.println("4. Cangkir");
        System.out.println("Jawab = ");
        int inputKonversi = input.nextInt();

        switch (inputKonversi) {
            case 1:
                coversiTeko(dataWadah[jenis], banyak);
                break;
            case 2:
                coversiBotol(dataWadah[jenis], banyak);
                break;
            case 3:
                coversiGelas(dataWadah[jenis], banyak);
                break;
            case 4:
                coversiCangkir(dataWadah[jenis], banyak);
                break;
            default:
        }

    }

    // teko ==> botol ==> gelas ==> cangkir
    //   1  <==   5   <==  10   <==   25
    //       *5       *2       *2,5

    private static void coversiTeko(String wadah, int jumlah) {
        double hasilKonversi = 0;
        String namaKonversi = dataWadah[0];
        if (wadah.equals(dataWadah[1])){
            hasilKonversi = (double) jumlah / 5;
        } else if (wadah.equals(dataWadah[2])) {
            hasilKonversi = (double) (jumlah / 2) / 5 ;
        }else if (wadah.equals(dataWadah[3])){
            hasilKonversi = ((jumlah / 2.5) / 2) / 5;
        }else {
            hasilKonversi = jumlah;
        }
        System.out.println(jumlah + " " + wadah + " = " + hasilKonversi + " " + namaKonversi);
    }

    // teko ==> botol ==> gelas ==> cangkir
    //   1  ==>   5   <==  10   <==   25
    //       *5         /2         /2,5
    private static void coversiBotol(String wadah, int jumlah) {
        double hasilKonversi = 0;
        String namaKonversi = dataWadah[1];
        if (wadah.equals(dataWadah[0])){
            hasilKonversi = (double) jumlah * 5;
        } else if (wadah.equals(dataWadah[2])) {
            hasilKonversi = (double) jumlah / 2;
        }else if (wadah.equals(dataWadah[3])){
            hasilKonversi = (jumlah / 2.5) / 2;
        }else {
            hasilKonversi = jumlah;
        }
        System.out.println(jumlah + " " + wadah + " = " + hasilKonversi + " " + namaKonversi);
    }

    // teko ==> botol ==> gelas ==> cangkir
    //   1  ==>   5   ==>  10   <==   25
    //       *5         *2         /2,5
    private static void coversiGelas(String wadah, int jumlah) {
        double hasilKonversi = 0;
        String namaKonversi = dataWadah[2];
        if (wadah.equals(dataWadah[0])){
            hasilKonversi = (double) (jumlah * 5) * 2;
        } else if (wadah.equals(dataWadah[1])) {
            hasilKonversi = (double) jumlah * 2;
        }else if (wadah.equals(dataWadah[3])){
            hasilKonversi = (jumlah / 2.5);
        }else {
            hasilKonversi = jumlah;
        }
        System.out.println(jumlah + " " + wadah + " = " + hasilKonversi + " " + namaKonversi);
    }

    // teko ==> botol ==> gelas ==> cangkir
    //   1  ==>   5   ==>  10   ==>   25
    //       *5       *2       *2,5
    private static void coversiCangkir(String wadah, int jumlah) {
        double hasilKonversi = 0;
        String namaKonversi = dataWadah[3];
        if (wadah.equals(dataWadah[0])){
            hasilKonversi = ((jumlah * 5) * 2) * 2.5;
        } else if (wadah.equals(dataWadah[1])) {
            hasilKonversi = (jumlah * 2) * 2.5;
        }else if (wadah.equals(dataWadah[3])){
            hasilKonversi = jumlah * 2.5;
        }else {
            hasilKonversi = jumlah;
        }
        System.out.println(jumlah + " " + wadah + " = " + hasilKonversi + " " + namaKonversi);
    }

}
