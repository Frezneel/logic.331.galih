package ProblemSolving14;

import java.util.Scanner;

public class Soal01 {
    public static void resolve(){
        Scanner input = new Scanner(System.in);
        // Input panjang deret
        System.out.println("+=====+ Deret Angka +=====+");
        System.out.println("Masukkan Input = ");
        int panjangDeret = input.nextInt();
        // Membuat Deret A = Kelipatan 3-1
        int[] deretA = new int[panjangDeret];
        System.out.println("Deret A : ");
        for (int i = 0; i < panjangDeret; i++) {
            deretA[i] = ( i * 3 ) - 1;
            System.out.print(deretA[i] + " ");
        }
        System.out.println();
        // Membuat Deret B = Kelipatan -2x1
        int[] deretB = new int[panjangDeret];
        System.out.println("Deret B : ");
        for (int i = 0; i < panjangDeret; i++) {
            deretB[i] = ( i * -2 ) * 1;
            System.out.print(deretB[i] + " ");
        }
        System.out.println();
        System.out.println("OUTPUT = ");
        for (int i = 0; i < panjangDeret; i++) {
            System.out.print((deretA[i] + deretB[i]) +" ");
        }

    }
}
