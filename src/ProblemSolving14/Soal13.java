package ProblemSolving14;

import java.util.Scanner;

public class Soal13 {
    public static void resolve(){
        char[] HURUF = "abcdefghijklmnopqrstuvwxyz".toCharArray();
        Scanner input = new Scanner(System.in);
        System.out.println("+=====+ Bobot Huruf +=====+");
        System.out.println("Masukkan data String = ");
        String inputString = input.nextLine().toLowerCase();
        char[] inputChar = inputString.toCharArray();
        System.out.println("Masukkan Array angka  = ");
        String inputArray = input.nextLine();
        String[] dataArray = inputArray.split(",");
        System.out.println("Output = ");
        String[] output = new String[dataArray.length];
        for (int i = 0; i < dataArray.length; i++) {
            int nilai = Integer.parseInt(dataArray[i]);
            char nilaiAsli = inputChar[i];
            if (HURUF[nilai-1] == nilaiAsli){
                output[i] = "true";
            }else {
                output[i] = "false";
            }
        }

        for (int i = 0; i < output.length; i++) {
            if (i == output.length-1){
                System.out.println(output[i]);
            }else {
                System.out.print(output[i] + ", ");
            }
        }
    }
}
