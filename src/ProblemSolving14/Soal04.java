package ProblemSolving14;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Scanner;

public class Soal04 {

    public static void resolve(){
        Scanner input = new Scanner(System.in);
        System.out.println("=====+ e-commerce +=====");

        // Input
        System.out.println("Uang Andi : ");
        int uang = input.nextInt();
        System.out.println("Jumlah barang : ");
        int jumlahBarang = input.nextInt();
        // bug
        input.nextLine();
        System.out.println("Masukkan Barang : ");
        String[][] barangArray = new String[2][jumlahBarang];

        for (int i = 0; i < jumlahBarang; i++) {
            String namaBarang = input.nextLine();
            String[] split = namaBarang.split(",");
            for (int j = 0; j < split.length; j++) {
                barangArray[j][i] = split[j];
            }
        }

        String result = "";
        //Tanpa Hashmap Olah Data
        for (int i = 0; i < jumlahBarang; i++) {
            String namaBarang = barangArray[0][i];
            int hargabarang = Integer.parseInt(barangArray[1][i]);
            if (uang >= hargabarang){
                uang -= hargabarang;
                result += namaBarang + ", ";
            }
        }

        System.out.println(result);

    }
}
