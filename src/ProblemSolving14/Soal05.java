package ProblemSolving14;

import java.util.Scanner;

public class Soal05 {

    public static void resolve() {
        Scanner input = new Scanner(System.in);
        System.out.println("+========+ Jam Converter +========+");
        System.out.println("Input jam = ");
        // Input
        String jamInput = input.nextLine().toUpperCase();
        int ambilJam = Integer.parseInt(jamInput.substring(0, 2));
        // 12:12 PM
        int ambilMenit = Integer.parseInt(jamInput.substring(3, 5));
        String output = "";
        //Olah
        if (jamInput.contains("AM") || jamInput.contains("PM")) {
            if (jamInput.contains("AM")) {
                output = jamInput.substring(0, 5);
            } else if (jamInput.contains("PM")) {
                if (ambilJam != 12){
                    output = (ambilJam + 12) + ":" + ambilMenit;
                }else {
                    output = "00" + ":" + ambilMenit;
                }
            }
        } else {
            if (ambilJam > 12) {
                output = (ambilJam - 12) + ":" + ambilMenit + "PM";
            } else {
                output = (ambilJam) + ":" + ambilMenit + "AM";
            }
        }

        System.out.println("Output = " + output);

    }
}
