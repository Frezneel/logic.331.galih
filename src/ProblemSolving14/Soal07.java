package ProblemSolving14;

import java.util.Scanner;

public class Soal07 {
    public static void resolve(){
        Scanner input = new Scanner(System.in);

        System.out.println("Masukkan Pola Lintasan");
        System.out.println("Contoh : ----o--o");
        System.out.println("input = ");
        String inputPola = input.nextLine();
        System.out.println("Masukkan Cara jalan");
        System.out.println("Contoh : wwwwjj");
        System.out.println("input = ");
        String inputJalan = input.nextLine();

        char[] polaChar = inputPola.toCharArray(); // Karena dibaca per karakter
        char[] jalanChar = inputJalan.toCharArray();
        int skip = 0; // untuk loncat
        int energy = 0; // energi ketika lompat
        String output = ""; // tampungan print out

        //olah data
        for (int i = 0; i < jalanChar.length; i++) {
            if ( i + skip >= polaChar.length){
                break;
            }
            char pola = polaChar[i + skip]; //Ambil data dulu
            char jalan = jalanChar[i];
            if (jalan == 'w' && pola == '-'){
                energy++;
                output = String.valueOf(energy);
            }else if (jalan == 'w' && pola == 'o'){
                output = "Jim Died";
                break;
            }
            else if (jalan == 'j' && (pola == '-' || pola == 'o') && energy >= 2){
                skip += 1;
                energy -= 2;
                output = String.valueOf(energy);
            }else if (jalan == 'j' && pola == 'o' && energy < 2){
                output = "Jim Died";
                break;
            }
        }

        System.out.println("Output : "+ output);
    }
}
