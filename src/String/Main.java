package String;


import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        menu();
    }
    public static void menu(){
        Scanner input = new Scanner(System.in);
        System.out.println("===== MENU =====");
        System.out.println("SOAL STRING MANIPULATION");
        System.out.println("Masukkan nomor soal dari 1 - 10");
        int pilihan = input.nextInt();
        while (pilihan < 1 || pilihan > 12){
            System.out.println("Pilihan tidak sesuai, Pastikan sesuai dengan Menu!");
            System.out.println("Pilihan : ");
            pilihan = input.nextInt();
        }

        switch (pilihan){
            case 1 :
                CamelCase01.resolve();
                break;
            case 2 :
                StrongPassword02.resolve();
                break;
            case 3 :
                CaesarCipher03.resolve();
                break;
            case 4 :
                MarsExploration04.resolve();
                break;
            case 5 :
                Soal05.resolve();
                break;
            case 6 :
                Panagrams06.resolve();
                break;
            case 7 :
                Separate07.resolve();
                break;
            case 8 :
                Gemstones08.resolve();
                break;
            case 9 :
                Anagrams09.resolve();
                break;
            case 10 :
                TwoStrings10.resolve();
                break;
            default:
        }
    }
}
