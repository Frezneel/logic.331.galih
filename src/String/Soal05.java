package String;

import java.util.Scanner;

public class Soal05 {
    private static String word = "hackerrank";
    public static void resolve(){
        Scanner input = new Scanner(System.in);
        System.out.println("======+ HakckerRank in a String +========");
        System.out.println("Contoh Input : hereiamstackerrank");
        System.out.println("Input 1 = ");
        String answer1 = input.nextLine();
        System.out.println("Input 2 = ");
        String answer2 = input.nextLine();

        //Merubah word dari string ke char
        char[] wordChar = word.toCharArray();
        char[] answerChar1 = answer1.toCharArray();
        char[] answerChar2 = answer2.toCharArray();

        String ouput1 = output(answerChar1, wordChar);
        String ouput2 = output(answerChar2, wordChar);

        System.out.println("Output 1= " + ouput1);
        System.out.println("Output 2= " + ouput2);
        Utility.mauLagi();
    }

    private static String output(char[] inputAnswer, char[] wordChar){
        int panjang = inputAnswer.length;
        int hilang = 0;
        String output = "";

        for (int i = 0; i < panjang; i++) {
            for (int j = hilang; j < wordChar.length; j++) {
                if (inputAnswer[i] == wordChar[j]){
                    hilang++;
                }
            }
            if (hilang == wordChar.length){
                output = "YES";
                break;
            }else {
                output = "NO";
            }
        }

        return output;
    }
}
