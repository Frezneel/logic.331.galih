package String;

import javax.swing.*;
import java.util.Scanner;

public class TwoStrings10 {
    public static void resolve(){
        Scanner input = new Scanner(System.in);
        System.out.println("======+ Two String +========");
        System.out.println("Input banyak pasangan : ");
        String pasanganInput = input.nextLine();
        int pasangan = Integer.parseInt(pasanganInput);
        String[][] data = new String[pasangan][2];

        for (int i = 0; i < pasangan; i++) {
            System.out.println("Masukkan input "+(i+1)+".1 = ");
            data[i][0] = input.nextLine();
            System.out.println("Masukkan input "+(i+1)+".2 = ");
            data[i][1] = input.nextLine();
        }
        boolean common = false;

        boolean[] commonData = new boolean[pasangan];
        // Data 1   | Data 2
        // A        | B
        for (int i = 0; i < data[0].length; i++) {
            common = false;
            // Ambil data 1
            String data1 = data[i][0];
            for (int j = 0; j < data1.length(); j++) {
                // Mengambil kata dari data 1
                String kataData1 = data1.substring(j, j+1);
                // Mendeklarasikan data 2
                String data2 = data[i][1];
                for (int l = 0; l < data2.length(); l++) {
                    // Ambil huruf pada data 2
                    String kataData2 = data2.substring(l,l+1);
                    if(kataData1.equals(kataData2)){
                        common = true;
                        break;
                    }
                }
                if (common){
                    break;
                }
            }

            if (common){
                commonData[i] = true;
            }else {
                commonData[i] = false;
            }
        }

        for (int i = 0; i < commonData.length; i++) {
            System.out.println("Data "+(i+1)+" = "+ commonData[i]);
        }

        Utility.mauLagi();
    }
}
