package String;

import java.util.Scanner;

public class StrongPassword02 {
//    private static String numbers = "0123456789";
//    private static String lower_case = "abcdefghijklmnopqrstuvwxyz";
//    private static String upper_case = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
    private static String special_characters = "!@#$%^&*()-+";
    public static void resolve(){

        Scanner input = new Scanner(System.in);

        System.out.println("======+ Strong Password +========");
        System.out.println("Contoh Input : #HackerRank999");
        System.out.print("Jawab = ");
        boolean isNumbers = false;
        boolean isLowerCase = false;
        boolean isUpperCase = false;
        boolean isSpecialCharacters = false;
        String answer = input.nextLine();
        int count = 4;
        int length = 0;
        char[] huruf = answer.toCharArray();
        char[] simbol = special_characters.toCharArray();


        if (huruf.length <= 6){
            length = 6 - huruf.length;
        }

        for (int i = 0; i < huruf.length; i++) {
            if (Character.isUpperCase(huruf[i]) && isUpperCase == false){
                isUpperCase = true;
                count--;
            }
            if (Character.isLowerCase(huruf[i])&& isLowerCase == false){
                isLowerCase = true;
                count--;
            }
            if (Character.isDigit(huruf[i]) && isNumbers == true){
                isNumbers = true;
                count--;
            }
            for (int j = 0; j < simbol.length; j++) {
                if (huruf[i] == simbol[j] && isSpecialCharacters == false){
                    isSpecialCharacters = true;
                    count--;
                }
            }
        }
        int output = 0;
        if (length <= count){
            output = count;
        }else {
            output = length;
        }


        System.out.println("Password bisa ditambahkan sebanyak = " + output);

        Utility.mauLagi();

    }
}
