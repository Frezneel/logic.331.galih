package String;

import java.util.Scanner;

public class Panagrams06 {
    final private static String huruf = "abcdefghijklmnopqrstuvwxyz";
    public static void resolve(){
        Scanner input = new Scanner(System.in);
        System.out.println("======+ Panagrams +========");
        System.out.println("Contoh Input : Saya Rajin Ngoding");
        System.out.println("Masukkan = ");
        String answer = input.nextLine();

        answer = answer.trim().toLowerCase();

        char[] answerChar = answer.trim().toCharArray();

        boolean output = cekPanagram(answerChar);

        if (output){
            System.out.println("Output = Panagrams");
        }else {
            System.out.println("Output = Bukan Panagrams");
        }
        Utility.mauLagi();
    }

    public static boolean cekPanagram(char[] input){
        char[] hurufChar = huruf.toCharArray();
        int panjangInput = input.length;
        int panjangHuruf = hurufChar.length;
        int hitungHuruf = 0;

        for (int i = 0; i < panjangHuruf; i++) {
            for (int j = 0; j < panjangInput; j++) {
                if (hurufChar[i] == input[j]){
                    hitungHuruf++;
                    break;
                }
            }
        }
        if (hitungHuruf == huruf.length()){
            return true;
        }else {
            return false;
        }

    }

}
