package String;

import java.util.Scanner;

public class MarsExploration04 {
    public static void resolve(){
        Scanner input = new Scanner(System.in);
        System.out.println("======+ Mars Exploration +========");
        System.out.println("- Pastikan input kelipatan dari 3");
        System.out.println("Contoh Input : SOSSPSSQSSOR");
        System.out.println("Masukkan = ");
        String answer = input.nextLine();
        int panjangAnswer = answer.length();
        boolean flag = true;
        while (flag){
            if (panjangAnswer % 3 != 0){
                System.out.println("Input harus kelipatan 3!");
                System.out.println("Masukkan : ");
                answer = input.nextLine();
                panjangAnswer = answer.length();
            }else {
                flag = false;
            }
        }
        answer = answer.toUpperCase();
        char[] hurufAnswer = answer.toCharArray();
        int countWrong = 0;
        int count = 0;
        for (int i = 0; i < panjangAnswer; i++) {
            if (count == 0){
                if (hurufAnswer[i] != 'S'){
                    countWrong++;
                }
                count++;
            }
            else if (count == 1) {
                if (hurufAnswer[i] != 'O'){
                    countWrong++;
                }
                count++;
            }
            else if (count == 2){
                if (hurufAnswer[i] != 'S'){
                    countWrong++;
                }
                count = 0;
            }
        }

        System.out.println("Output (Tidak Sesuai) = " + countWrong);
        Utility.mauLagi();
    }
}
