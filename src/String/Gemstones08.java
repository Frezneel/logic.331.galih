package String;

import java.util.Scanner;

public class Gemstones08 {
    private static String originalAlphabet = "abcdefghijklmnopqrstuvwxyz";
    public static void resolve() {
        Scanner input = new Scanner(System.in);
        System.out.println("======+ Gemstone +========");
        System.out.println("Masukkan banyak input = ");
        String answer = input.nextLine();
        int banyak = Integer.parseInt(answer);
        String[] allAnswer = new String[banyak];

        for (int i = 0; i < banyak; i++) {
            System.out.println("Masukkan input ke-"+ (i+1) +" :");
            allAnswer[i] = input.nextLine();
        }
        int output = findGemstone(allAnswer);
        System.out.println("Output Gemstone = " + output);
        Utility.mauLagi();
    }

    private static int findGemstone(String[] allAnswer) {
        int count = 0;
        String pembanding = allAnswer[0];
        //Ambil data pertama
        for (int i = 0; i < pembanding.length(); i++) {
            // Dibandingkan dengan input ke 2 sampai dengan n
            String hurufPembanding = pembanding.substring(i,i+1);
            boolean[] isSame = new boolean[(allAnswer.length) - 1];
            for (int j = 1; j < allAnswer.length; j++) {
                for (int k = 0; k < allAnswer[j].length(); k++) {
                    String inputLain = allAnswer[j];
                    String hurufLain = inputLain.substring(k,k+1);
                    if (hurufPembanding.equals(hurufLain)){
                        isSame[(j-1)] = true;
                        break;
                    }
                }
            }
            boolean equals = isSame[0];
            for (int j = 1; j < isSame.length; j++) {
                equals &= isSame[j];
            }
            if (equals){
                count++;
            }
        }
        return count;
    }
}
