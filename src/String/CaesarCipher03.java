package String;

import java.util.Scanner;

public class CaesarCipher03 {
    private static String originalAlphabet = "abcdefghijklmnopqrstuvwxyz";
    private static String originalUppercase = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
    private static String special_characters = "!@#$%^&*()-+ ";

    public static void resolve(){
        Scanner input = new Scanner(System.in);
        System.out.println("======+ Caesar Chiper +========");
        System.out.println("Contoh Input : Middle-Outz");
        System.out.println("Jawab = ");
        String answer = input.nextLine();
        System.out.println("Masukkan besar rotasi = ");
        System.out.println("Jawab = ");
        int rotated = input.nextInt();

        String output = "";

        boolean isSimbol = false;
        char[] hurufAnswer = answer.toCharArray();
        char[] hurufOriginal = originalAlphabet.toCharArray();
        char[] simbol = special_characters.toCharArray();
        char[] hurufUppercase = originalUppercase.toCharArray();

        for (int i = 0; i < hurufAnswer.length; i++) {
            for (int j = 0; j < simbol.length; j++) {
                if (hurufAnswer[i] == simbol[j]){
                    isSimbol = true;
                }
            }
            if (!isSimbol){
                for (int j = 0; j < hurufOriginal.length; j++) {
                    if (hurufAnswer[i] == hurufOriginal[j]){
                        if (j > ((hurufOriginal.length-1) - rotated)){
                            int posisi = ((j + rotated) - (hurufOriginal.length));
                            output += hurufOriginal[posisi];
                        }else {
                            output += hurufOriginal[j + rotated];
                        }
                    }
                    if (hurufAnswer[i] == hurufUppercase[j]){
                        if (j > ((hurufUppercase.length-1) - rotated)){
                            int posisi = ((j + rotated) - (hurufUppercase.length));
                            output += hurufUppercase[posisi];
                        }else {
                            output += hurufUppercase[j + rotated];
                        }
                    }
                }
            }else {
                output += hurufAnswer[i];
                isSimbol = false;
            }

        }

        System.out.println("Hasil Konversi Caesar Cipher = " + output);
        Utility.mauLagi();
    }
}
