package String;

import java.util.Scanner;

public class CamelCase01 {
    public static void resolve(){
        Scanner input = new Scanner(System.in);

        System.out.println("======+ CamelCase +========");
        System.out.println("Contoh Input : saveChangesInTheEditor");
        System.out.print("Jawab = ");
        String answer = input.nextLine();
        int count = 1;

        // Merubah dari String ke char, char itu ambiil per karakter.
        // Contoh : Input = sayaSayang
        // jadi char = [s][a][y][a][S][a][y][a][n][g]
        char[] huruf = answer.toCharArray();

        for (int i = 0; i < huruf.length; i++) {
            if (Character.isUpperCase(huruf[i])){
                count++;
            }
        }

        System.out.println("Banyak Kata = " + count);

        Utility.mauLagi();

    }


}
