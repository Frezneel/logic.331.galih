package String;

import java.util.Scanner;

public class Anagrams09 {
    public static void resolve(){
        Scanner input = new Scanner(System.in);
        System.out.println("======+ Anagrams +========");
        System.out.println("Masukkan input 1 = ");
        String input1 = input.nextLine();
        input1 = input1.toLowerCase();
        System.out.println("Masukkan input 2 = ");
        String input2 = input.nextLine();
        input2 = input2.toLowerCase();
        int hitung = 0;

//        char[] inputChar1 = input1.toCharArray();
//        char[] inputChar2 = input2.toCharArray();

        int[] huruf = new int[26];

        // Ambil posisi Acsii dikurang nilai Ascii 'A' agar nilai mulai dari 0
        for (char c : input1.toCharArray()) {
            huruf[c - 'a']++;
        }

        // Ambil posisi Acsii dikurang nilai Ascii 'A' agar nilai mulai dari 0
        // Mengembalikan nilai dari 1 ke 0
        for (char c : input2.toCharArray()) {
            huruf[c - 'a']--;
        }

        // Menghitung banyaknya nilai 1 pada Array
        for (int c : huruf) {
            hitung += Math.abs(c);
        }

        System.out.println("Output = "+ hitung);
        Utility.mauLagi();
    }
}
